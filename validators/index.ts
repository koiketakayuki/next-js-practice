import { InputValue } from "../hooks/formItem";

export const validateName = (value: InputValue<string>) => {
  if (value === "") {
    return ["氏名を入力してください。"];
  }
  if ([...value].length > 255) {
    return ["氏名は255文字以内で入力してください。"];
  }
  return [];
};

export const validateEmail = (value: InputValue<string>) => {
  if (value === "") {
    return ["メールアドレスを入力してください。"];
  }
  if (!value.includes("@")) {
    return ["メールアドレスには@を含めてください。"];
  }
  return [];
};

export const validatePassword = (value: InputValue<string>) => {
  if (value === "") {
    return ["パスワードを入力してください。"];
  }
  return [];
};

export const validatePasswordForConfirmation = (
  value: InputValue<string>,
  options: {
    password: InputValue<string>
  }
) => {
  if (value === "") {
    return ["パスワード（確認用）を入力してください。"];
  }
  if (value !== options.password) {
    return ["パスワードとパスワード（確認用）を一致させてください。"];
  }
  return [];
};

export const validateAge = (value: InputValue<number>) => {
  if (value === "") {
    return ["年齢を入力してください。"];
  }
  return [];
};

export const validateGender = (value: InputValue<string>) => {
  if (value === "") {
    return ["性別を入力してください。"];
  }
  return [];
};

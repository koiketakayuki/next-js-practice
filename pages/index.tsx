import { NextPage } from "next";
import { Errors } from "../components/Errors";
import { useForm } from "../hooks/form";
import { useFormItem } from "../hooks/formItem";
import {
  validateName,
  validateEmail,
  validatePassword,
  validatePasswordForConfirmation,
  validateAge,
  validateGender
} from "../validators";

const Index: NextPage = () => {
  const name = useFormItem<string>("", validateName);
  const email = useFormItem<string>("", validateEmail);
  const password = useFormItem<string>("", validatePassword);
  const passwordForConfirmation = useFormItem<string>(
    "",
    validatePasswordForConfirmation
  );
  const age = useFormItem<number>("", validateAge);
  const gender = useFormItem<string>("", validateGender);

  const form = useForm({
    name,
    email,
    password,
    age,
    gender
  });

  return (
    <>
      <form
        onSubmit={e => {
          e.preventDefault();
          form.dirtyAll();
          console.log(form.hasErrors);
        }}
      >
        <div>
          <label>
            <span>氏名</span>
            <input
              value={name.value}
              onChange={name.onChangeTextInput}
              onBlur={name.onBlur}
            />
          </label>
          {name.hasError && name.isDirty && <Errors errors={name.errors} />}
        </div>

        <div>
          <label>
            <span>メールアドレス</span>
            <input
              type="email"
              value={email.value}
              onChange={email.onChangeTextInput}
              onBlur={email.onBlur}
            />
          </label>
          {email.hasError && email.isDirty && <Errors errors={email.errors} />}
        </div>

        <div>
          <label>
            <span>パスワード</span>
            <input
              type="password"
              value={password.value}
              onChange={password.onChangeTextInput}
              onBlur={password.onBlur}
            />
          </label>
          {password.hasError && password.isDirty && (
            <Errors errors={password.errors} />
          )}
        </div>

        <div>
          <label>
            <span>パスワード（確認用）</span>
            <input
              type="password"
              value={passwordForConfirmation.value}
              onChange={passwordForConfirmation.onChange}
              onBlur={passwordForConfirmation.onBlur}
            />
          </label>
          {passwordForConfirmation.hasError &&
            passwordForConfirmation.isDirty && (
              <Errors errors={passwordForConfirmation.errors} />
            )}
        </div>

        <div>
          <label>
            <span>年齢</span>
            <input
              type="number"
              value={age.value}
              onChange={age.onChangeNumberInput}
              onBlur={age.onBlur}
            />
          </label>
          {age.hasError && age.isDirty && <Errors errors={age.errors} />}
        </div>

        <div>
          <label>
            <span>性別</span>
            <select
              value={gender.value}
              onChange={gender.onChangeSelect}
              onBlur={gender.onBlur}
            >
              <option value=""></option>
              <option value="male">男</option>
              <option value="female">女</option>
            </select>
          </label>
          {gender.hasError && gender.isDirty && (
            <Errors errors={gender.errors} />
          )}
        </div>

        <button type="submit">SUBMIT</button>
      </form>
    </>
  );
};

export default Index;

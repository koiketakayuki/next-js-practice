interface IProps {
  errors: string[];
}

export const Errors: React.FC<IProps> = props => {
  return (
    <ul>
      {props.errors.map((error, index) => (
        <li key={index}>
          <span>{error}</span>
        </li>
      ))}
    </ul>
  );
};

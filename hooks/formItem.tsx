import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";

export type InputValue<T> = T | "";
export type OnChangeTextInput = (e: ChangeEvent<HTMLInputElement>) => void;
export type OnChangeNumberInput = (e: ChangeEvent<HTMLInputElement>) => void;
export type OnChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => void;

export interface IFormItem<T> {
  value: InputValue<T>;
  errors: string[];
  hasError: boolean;
  isDirty: boolean;
  setIsDirty: Dispatch<SetStateAction<boolean>>;
  onChangeTextInput: OnChangeTextInput;
  onChangeNumberInput: OnChangeNumberInput;
  onChangeSelect: OnChangeSelect;
  onBlur: () => void;
}

export function useFormItem<V, O = undefined>(
  defaultValue: InputValue<V>,
  validator: (value: InputValue<V>, options?: O) => string[],
  validatorOptions?: O
): IFormItem<V> {
  const [value, setValue] = useState<InputValue<V>>(defaultValue);
  const [errors, setErrors] = useState<string[]>(
    validator(defaultValue, validatorOptions)
  );
  const [isDirty, setIsDirty] = useState(false);

  const onChangeTextInput = (e: ChangeEvent<HTMLInputElement>) => {
    setValue((e.target.value as unknown) as InputValue<V>);
    setErrors(
      validator((e.target.value as unknown) as InputValue<V>, validatorOptions)
    );
  };
  const onChangeNumberInput = (e: ChangeEvent<HTMLInputElement>) => {
    const parsedValue = parseInt(e.target.value, 10);
    const value = isNaN(parsedValue) ? "" : parsedValue;
    setValue((value as unknown) as InputValue<V>);
    setErrors(validator((value as unknown) as InputValue<V>, validatorOptions));
  };
  const onChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    setValue((e.target.value as unknown) as InputValue<V>);
    setErrors(
      validator((e.target.value as unknown) as InputValue<V>, validatorOptions)
    );
  };

  const onBlur = () => {
    setIsDirty(true);
  };

  return {
    value,
    errors,
    hasError: errors.length > 0,
    isDirty,
    setIsDirty,
    onChangeTextInput,
    onChangeNumberInput,
    onChangeSelect,
    onBlur
  };
}

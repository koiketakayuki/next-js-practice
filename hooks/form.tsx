import { IFormItem } from "./formItem";

interface IFormData {
  [key: string]: IFormItem<any>;
}

export interface IForm {
  hasErrors: boolean;
  dirtyAll: () => void;
}

export const useForm = (formData: IFormData): IForm => {
  const keys = Object.keys(formData);

  return {
    hasErrors: keys.some(key => formData[key].hasError),
    dirtyAll: () => {
      keys.forEach(key => formData[key].setIsDirty(true));
    }
  };
};
